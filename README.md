# script.service.playbackresumer
## Simple addon to update the resume point of the currently playing video automatically in the background and optional resume from it when xbmc is restarted after a crash.

http://forum.kodi.tv/showthread.php?tid=183084

Imported from the old [Google Code source](https://code.google.com/archive/p/xbmc-bradvido-repository/)

Not maintained by me or anyone, and tbh I have no idea if it even works.

But I saw that the old link was dead and decided to upload it for others.